# gitlab-afs

gitlab-afs is a package for integrating [gitlab](https://about.gitlab.com/)
with [OpenAFS](https://openafs.org/); specifically, by making it possible for
a site sysadmin to store repositories in an AFS volume. Notably, gitlab-afs
also makes it possible for users to clone repositories over AFS, meaning
that even if the gitlab instance is unavailable for whatever reason, code
can still be checked out as long as a user is in possession of valid AFS
credentials.

gitlab-afs consists of two main components:

* gitlab-kerberos: a runit service (intended for installation on top of an
omnibus-gitlab installation, but can also be installed anywhere) which ensures
the "git" user has and keeps Kerberos tokens. 

* gitlab-afs: a Ruby program that uses the Gitlab API to derive a list of
users who have read-write access to all repositories, and grants those users
the ability to check out those repositories over AFS.

Note that gitlab-afs assumes a 1 to 1 mapping between gitlab users at your
site and AFS/Kerberos accounts.

## gitlab-kerberos

For more on gitlab-kerberos, take a look at [this README](runit/README.md).

## gitlab-afs

### Installation

gitlab-afs is packaged as a Ruby gem. To install it, you can checkout the
code and run ```gem build gitlab-afs/gitlab-afs.gemspec```, or build it whatever
other way people build Ruby gems. Or, you can download one from the repository;
gitlab CI is set up to build gems on each commit to master.

### Configuration

In order to run ```gitlab-afs```, you need a configuration file. By default,
we look for a configuration file in ```/etc/gitlab-afs.conf```, and if one
is not found there, we check instead for one in ```./gitlab-afs.conf``` (i.e.
the current directory). If you wish to run over a config file located elsewhere,
pass ```-c CONFIG_LOCATION``` when running the script.

The configuration file must have the following fields:

```
[gitlab]
repository_path = # Location of the repositories in AFS
api_url = # The API endpoint of your gitlab instance, probably the URL followed by /api/v3

[auth]
user = # The user to connect to
token = # The user's authentication token
```

### Usage

You must, obviously, have a working OpenAFS client on your system. It is assumed
that the user is running with Kerberos tickets and AFS tokens for a user who has
the power to set permissions (the "a" flag in the OpenAFS ACL) over all sub-directories
of the repository path.

## Credits

* Ben Rosser <bjr@acm.jhu.edu>
