require 'rake'

Gem::Specification.new do |s|
  s.name        = 'gitlab-afs'
  s.version     = '0.1.1'
  s.date        = '2017-11-29'
  s.summary     = "Gitlab integration with OpenAFS"

  s.description = "Uses the Gitlab API to integrate Gitlab with OpenAFS by
                   setting the AFS permissions on the repositories such that users
                   can clone them directly over AFS, instead of over HTTPS/SSH."

  s.authors     = ["Ben Rosser"]
  s.email       = 'bjr@acm.jhu.edu'


  s.files = FileList['lib/*/*.rb', 'bin/*'].to_a
  s.bindir = 'bin'
  # Hardcode these for now.
  s.executables = ['gitlab-afs']

  s.homepage    = 'https://gitlab.com/TC01/gitlab-afs/'
  s.license       = 'MIT'

  s.add_runtime_dependency 'gitlab'
  s.add_runtime_dependency 'parseconfig'

end
