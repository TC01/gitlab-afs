# gitlab-kerberos

gitlab-kerberos is an example runit service intended to be added to the
default omnibus-gitlab distribution. It consists of two shellscripts that
ensure the "git" user on the system (the user that gitlab runs as) always
has Kerberos tickets. gitlab-kerberos makes a number of assumptions, including
that:

* You want to give gitlab Kerberos tickets that are stored in ```/etc/krb5.keytab```.

* Gitlab is, in fact, running as the "git" user (probably always true under
a default omnibus-gitlab installation).

* You are using omnibus-gitlab and installing this runit service there.

Since gitlab-kerberos is more or less just a few lines of bash, I've decided
to not attempt to make these assumptions configurable; the scripts are easy
enough to read and adjust if you want to install this service under /etc/service
instead of /opt/gitlab/service, for example.

## Setup

You need the program "k5start" (usually packaged as "kstart") installed,
as well as working MIT Kerberos and OpenAFS clients.

You also need to create a Kerberos keytab and put it somewhere on the system.
You'll want whatever Kerberos/AFS principal this keytab represents to have tickets
for a user that has rlidwka access to the git repository path in OpenAFS.

You will then want to set the following options in your gitlab configuration
(again, assuming the ominbus installation).

```
# Path to the repositories in AFS.
gitlab_rails['gitlab_shell_repos_path'] = "/afs/.../repositories"

# Don't try to manage the storage directories manually.
manage_storage_directories['enable'] = false
```

## Credits

* Ben Rosser <bjr@acm.jhu.edu>

* Nathaniel Wes Filardo <nwf@acm.jhu.edu>: wrote the shellscripts used to
keep Kerberos tickets for a similar purpose (to ensure that Apache would be able
to serve files out of AFS).
