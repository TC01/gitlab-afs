#!/bin/sh
USR=$1
export AKLOG="su ${USR} -s /bin/sh -c /usr/bin/aklog"
exec k5start -U -t -K 600 -l 12h -k KEYRING:user -f /etc/krb5.keytab
