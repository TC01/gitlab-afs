# gitlab-afs configuration parser
# Uses ruby parseconfig module, provides thin wrapper around it

require 'parseconfig'

module GitlabAFSConfig

	# XXX: todo: add checks here.
	def self.read_config(path)
		config = ParseConfig.new(path)
		return config
	end

	def self.get_admin_user(config)
		return config['auth']['user']
	end

	def self.get_admin_token(config)
		return config['auth']['token']
	end

	def self.get_api_url(config)
		return config['gitlab']['api_url']
	end

	def self.get_repository_path(config)
		return config['gitlab']['repository_path']
	end

end
