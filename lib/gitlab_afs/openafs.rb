# Ruby wrapper around 'fs sa' for gitlab purposes.

require 'find'

# Generic implementation of fs sa and fs sar.
module OpenAFSWrapper

	def self.fs_sa(user, acl, dir)
		Kernel.system("fs sa -dir " + dir + " -acl " + user + " " + acl)
	end

	def self.fs_sa_recursive(user, acl, dir)
		Find.find(dir) do |subdir|
			if File.directory? subdir
				OpenAFSWrapper.fs_sa(user, acl, subdir)
			end
		end
		# Do this last, so if it happened, we know the process finished.
		OpenAFSWrapper.fs_sa(user, acl, dir)
	end

	def self.fs_la(dir)
		output = `fs la #{dir}`
		return output		
	end

end

# Implementation of the gitlab AFS use case.
module GitlabAFSWrapper
		
	def self.setacl(user, path, namespace, project_name)
		namespace_path = File.join(path, namespace)
		project_path = File.join(namespace_path, project_name + ".git")
		OpenAFSWrapper.fs_sa(user, 'l', namespace_path)
		OpenAFSWrapper.fs_sa_recursive(user, 'rl', namespace_path)
	end

	def self.get_users_and_acls(path, namespace, project_name)
		namespace_path = File.join(path, namespace)
		dir = File.join(namespace_path, project_name + ".git")
		
		output = OpenAFSWrapper.fs_la(dir)
		text, aclstring = output.split("Normal rights:\n")
		aclstrings = aclstring.split("\n")
		results = {}
		# This syntax is ugly!
		aclstrings.each {
			|acl|
			user, mode = acl.lstrip().split(" ")
			results[user] = mode
		}
		return results
	end

	def self.get_project_path(path, namespace, project_name)
		namespace_path = File.join(path, namespace)
		dir = File.join(namespace_path, project_name + ".git")
		return dir
	end
	
end
