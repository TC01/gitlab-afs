# Main module of gitlab-afs.

require 'gitlab'

require 'gitlab_afs/config'
require 'gitlab_afs/openafs'

module GitlabAFS
	
	def self.main(config_name, force)
		# Now try to read the config file!
		begin
			config = GitlabAFSConfig.read_config(config_name)
		rescue Errno::EACCES => e
			print("WARNING: Failed to find config file, checking for ./gitlab-afs.conf\n")
			begin
				config = GitlabAFSConfig.read_config("./gitlab-afs.conf")
			rescue Errno::EACCES => e
				print("ERROR: Failed to find ./gitlab-afs.conf, aborting. Please pass -c PATH_TO_CONFIG_FILE and rerun.\n")
				return
			end
		end

		# This is kind of stupid.
		token = GitlabAFSConfig.get_admin_token(config)
		admin_user = GitlabAFSConfig.get_admin_user(config)
		path = GitlabAFSConfig.get_repository_path(config)
		endpoint = GitlabAFSConfig.get_api_url(config)

		# Configure gitlab.
		print("Connecting to gitlab instance running at " + endpoint + "...\n")

		Gitlab.configure do |config|
			config.endpoint = endpoint
			config.private_token = token
			config.sudo = admin_user
		end

		# Load gitlab projects.
		print("Connected. Loading list of all projects...\n")
		projects = Gitlab.projects(scope: 'all')
		projects.auto_paginate do |project|
			
			namespace = project.namespace.name
			project_name = project.name
			
			# Do some processing.
			project_name.gsub!(' ', '-')
			namespace = namespace.downcase()
			project_name = project_name.downcase()
			
			users = Gitlab.team_members(project.id)

			begin
				group_users = Gitlab.group_members(project.namespace.id)
			rescue
				group_users = []
			end	

			usernames = []
			for user in users
				usernames << user.username
			end

			for user in group_users
				if not usernames.include? user.username
					usernames << user.username
				end
			end
			
			# Delete users who we want to delete.
			usernames.delete('root')
			
			# If a directory doesn't even exist, just continue.
			project_path = GitlabAFSWrapper.get_project_path(path, namespace, project_name)
			if not File.directory? project_path
				next
			end
			
			# Don't override existing permissions, unless told to of course.
			if not force
				current_acls = GitlabAFSWrapper.get_users_and_acls(path, namespace, project_name)
				current_acls.each do |key, value|
					usernames.delete(key)
				end
			end
			
			for user in usernames
				print("Granting user " + user + " read access to " + project.namespace.name + "/" + project.name + "\n")
				GitlabAFSWrapper.setacl(user, path, namespace, project_name)
			end
		end

	end
end
